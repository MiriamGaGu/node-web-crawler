const http = require('http')
const fs = require('fs')
const path = require('path')
const uuidv5 = require('uuid/v5');

const downloadpage = async (URL) => {
    console.log('Your file is Downloading' + URL);

    // generar valor random para el folder que este còdigo que va a generar
    const folder = await uuidv5(URL, uuidv5.URL)
    // 
    fs.mkdir(`./${folder}`, { recursive: true }, (err) => {
        if (err) throw err
    });
    // elcallback va a retornar la info cuando se ejecute la funcion fetchPage
    const fetchPage = (linkUrl, callback) => {

        let content = "";
        // se hace una solicitud a la url que recibimos como argumento
        const req = http.request(linkUrl, function (res) {
            // esta es la respuesta  la solicitud
            res.setEncoding("utf8");
            // aqui recibimos la data del html
            res.on("data", function (chunk) {
                content += chunk;
            });
            // cse ejecuta esta funcion cuando la respuesta es positiva, regresa el callback
            res.on("end", function () {
                callback(folder, content, URL)
            })

        });
        //Regresa el error si el req es negativo
        req.on('error', e => console.error(e))
        //Aqui se cierra el request
        req.end();
    }
    // este es el callback que pasara como argumento en fetchPage, fetchPage contiene los 3 argumentos que le pasamos
    // folder(que fue creado con uuid, content que trae la data de la url y la url )
    const write = (carpet, data, urlData) => {
        fs.writeFile(`./${carpet}/file.html`, data, (err) => {
            if (err) throw err;
        });
        fs.writeFile(`./${carpet}/url.txt`, urlData, (err) => {
            if (err) throw err;
        });

        console.log('Downloading is done in folder' + carpet);
    }

    fetchPage(URL, write);
}
// estos seran los argumentos que se pasaran en la terminal
//Note:en el index se pasara el 2 porque los dos primeros son el mòdulo de node, el segundo es el codigo en si
//y el tercero es el url del array que regresa. (node[0] download.js[1] url[2])
downloadpage(process.argv[2]);



